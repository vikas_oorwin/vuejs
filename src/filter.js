import Vue from 'vue';

const uppercase = Vue.filter('uppercase', value=>{
    return value.toUpperCase();
});

export default{
    uppercase
}
