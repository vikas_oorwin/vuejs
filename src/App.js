import Header from './views/Header.vue';
export default {
  name: 'App',
  data() {
    return {
        postsCount: 100
    }
  },
  components: {
    'app-header': Header,
  }
}