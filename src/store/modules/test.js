const state = {
    counter: 0,
    testData: 'hello',
    sampleData: 'sample'
};

const getters = {
    testData: (state,getters,rootState) => {
        console.log(rootState);
        return state.testData
    }
};

const mutations = {
    getSampleData(state,data) {
        console.log(data);
        state.sampleData = data;
    }
};

const actions = {
    getSampleData({commit},val) {
        commit('getSampleData', val);
    }


};

export default{
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}