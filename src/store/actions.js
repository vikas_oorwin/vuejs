import axios from 'axios';
export const actions = {
    getTeamData({commit}){
        axios.get('https://reqres.in/api/users?page=1')
        .then(response=>{
            commit('getTeamData', response.data.data)
        });
    }
}