import axios from 'axios';
export default{
    data() {
        return{
            email: '',
            password: '',
            age:'',
            country:'usa',
            id: this.$route.params.id, 
        }
    },
    created() {
        if(typeof(this.id) != 'undefined') {
            axios.get('/users/'+this.id+'.json')
            .then(response=>{
                let user_data = response.data;
                this.email = user_data.email;
                this.age = user_data.age;
                this.password = user_data.password;
                this.country = user_data.country;
            });
        }

    }
}