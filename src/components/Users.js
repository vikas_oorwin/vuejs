
import axios from 'axios';
import {mapGetters} from 'vuex';
export default{
    data() {
        return {
            userData: [],
            counter: this.$store.state.test.counter,
        }
    },
    computed: {
        ...mapGetters(['teams']),
        ...mapGetters('test',['testData']),
        teamData() {
            return this.$store.state.teamData
        },
        // sampleData() {
        //     console.log("hello");
        // }
        sampleData: {
            get() {
                return 'hi'
            },
            set(value) {
                console.log(value);
                return this.$store.dispatch('test/getSampleData',value);
            }
        }
    },
    created() {
        axios.get('/users.json')
        .then(response=>{
            const data = response.data;
            const users  = [];
            for(let key in data) {
                const user = data[key];
                user.id = key;
                users.push(user);
            }
            this.userData = users;
        });
    },
    mounted() {
        this.$store.dispatch('getTeamData');
        // this.$store.dispatch('test/getSampleData');
    }
}