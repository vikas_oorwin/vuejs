import axios from 'axios';
import {required, minLength, between} from 'vuelidate/lib/validators';
export default{
    data() {
        return {
            email: '',
            password: '',
            age:'',
            country:'usa',
            id: this.$route.params.id,
        }
    },
    validations: {
        email: {
            required,
            minLength: minLength(30),
        },
        age: {
            between: between(20,30),
        }

    },
    methods: {
        onSubmit() {
            this.$v.$touch();
            console.log(this.$v);
            if(this.$v.$error) {
                console.log(this.$v);
                return;
            }
            const formData = {
                email: this.email,
                age:this.age,
                password: this.password,
                country: this.country
            };

            axios.post('/users.json', formData)
            .then(function(res){
                console.log(res);
            });

            this.$router.push('/users');
        }
    },
    created() {
        if(typeof(this.id) != 'undefined') {
            axios.get('/users/'+this.id+'.json')
            .then(response=>{
                let user_data = response.data;
                this.email = user_data.email;
                this.age = user_data.age;
                this.password = user_data.password;
                this.country = user_data.country;
            });
        }
    }
}