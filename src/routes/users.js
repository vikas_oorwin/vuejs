export const users = [
    {path:'',component : () => import('../views/Home.vue'), name: 'Home'},
    {path:'/users', component : () => import('../views/Users.vue'), name: 'UsersList'},
    {path:'/users/create', component:() => import('../views/UserCreate.vue'), name: 'UserCreate'},
    {path:'/users/:id/edit', component:() => import('../views/UserCreate.vue'), name: 'UserEdit'},
    {path:'/users/:id/view', component:() => import('../views/UserView.vue'), name: 'UserView'}
    
]