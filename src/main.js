import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import {routes} from './routes.js';
import axios from 'axios';
import store from './store/index.js';
import filters from './filter.js';
import Vuelidate from 'vuelidate';

console.log(process.env.VUE_APP_TITLE);
console.log(process.env.VUE_APP_NAME);
console.log(process.env);
Vue.config.productionTip = false;
Vue.use(Vuelidate);

axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;

Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

new Vue({
  router,
  store,
  filters,
  render: h => h(App),
}).$mount('#app')
